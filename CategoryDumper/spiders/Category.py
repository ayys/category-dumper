# -*- coding: utf-8 -*-
import scrapy
import os

import logging
import logging.config
import ast


# CONFIGURATION FILE
CURRENT_DIR = os.getcwd()

# Configuration Parameters
MAIN_URL = "main_url"  # the starting point of the crawler
CATEGORY_NAME = "category_name"  # xpath containing a category name
CATEGORY_PATH = "category_path"  # xpath containing a category link
SUBCATEGORY_NAME = "subcategory_name"  # xpath containing a subcategory name
SUBCATEGORY_PATH = "subcategory_path"  # xpath containing a subcategory link
CATEGORY_RESULTS = "category_results"
SUBCATEGORY_RESULTS = "subcategory_results"
PART_URL="part_url" # string containing inital url path
URL_INIT = "url_init"

import pudb
pudb.set_trace()


def clean_string(string):
    '''
    strips spaces and tabs from end of strings
    '''
    for each in [' ', '\t']:
        string = string.strip(each)
        string = string.lstrip(each)
        string = string.rstrip(each)
    string = string.replace("\n", "")
    return string


def get_subcategory(category, subcategory_xpaths):
    '''
    get subcategory from the same page as the category
    '''
    subcategory_results = category.xpath(subcategory_xpaths['results'])

    for subcategory in subcategory_results:
        subcategory_name = "".join(subcategory.xpath(subcategory_xpaths['name']).extract())
        subcategory_path = subcategory.xpath(subcategory_xpaths['path']).extract_first()
        yield {
                "subcategory_name": subcategory_name,
                "link": subcategory_path
               }


class CategorySpider(scrapy.Spider):
    '''
    __Category Dumper__ is a configurable crawler which is made to crawl
    job related sites and scrape the categories adn sub-categories.
    The focus is given on websites in which the category and sub-category
    are on different pages. Thus, categoryDumper always follows the link
    given by the category.
    The output of the program is undefined for pages where the category and
    subcategory are on the same page.
    '''

    name = "categorydumper"

    def __init__(self, filename, log_config_file="logging_config.ini"):

        __CONFIG_FILE__ = "configs/" + filename  # internal variable
        # load config file as a python dict in a safe way
        self.config = ast.literal_eval(
                open(__CONFIG_FILE__).read())
        self.config_keys = self.config.keys()
        self.url_init = self.config.get(URL_INIT, "")
        self.start_urls = self.config.get(MAIN_URL, '')

        self.output = None

    def parse(self, response):
        '''
        automatically called by scrapy to parse the page.
        Parse does the following :

        1. get a list of results.
        2. Find the category name and link in the result
        3. If there is a sub category, follow the category link
        3.a. If category and subcategory are in same page, use
                apply subcategory xpath to the category selector
        4. yield output
        '''

        # getting list of categories
        category_results = response.xpath(
                self.config.get(CATEGORY_RESULTS, ''))
        for category in category_results:
            # category name
            category_name = category.xpath(
                    self.config.get(CATEGORY_NAME, "")).extract_first()
            category_name = clean_string(category_name)
            # category link
            category_path = category.xpath(
                    self.config.get(CATEGORY_PATH, "")).extract_first()
            category_path = clean_string(category_path)
            # if part_url is available then, add it to category link
            if 'part_url' in self.config_keys:
                category_path=self.config.get(PART_URL, "")+category_path
            # if only_category parameter is enabled in the configuration,
            # then don't check for sub categories.
            # Else, goto parse_subcategories
            #
            category_dict = {
                    'category': category_name,
                    'link': category_path,
                    'url_init': self.url_init
                    }
            if 'only_category' in self.config_keys:
                yield category_dict
            else:
                if 'subcategory_in_same_page' in self.config_keys:
                    subcategories = get_subcategory(category, {
                        "results":    self.config[SUBCATEGORY_RESULTS],
                        "name":    self.config[SUBCATEGORY_NAME],
                        "path":    self.config[SUBCATEGORY_PATH]
                        })
                    for subcategory in subcategories:
                        category_dict.update(subcategory)
                        yield category_dict
                else:
                    yield scrapy.Request(
                            url=category_path,
                            callback=self.parse_subcategory,
                            dont_filter=True,
                            meta={'category': category_name})

    def parse_subcategory(self, response):
        '''
        Similar to `parse`, but parses sb categories and yields the
        output. The meta data given by the parse function contains the
        category name, which is also yielded by this method
        '''
        subcategory_results = response.xpath(
                self.config.get(SUBCATEGORY_RESULTS, ''))

        for subcategory in subcategory_results:
            subcategory_name = subcategory.xpath(
                    self.config.get(SUBCATEGORY_NAME, "")).extract_first()
            subcategory_name = clean_string(subcategory_name)
            subcategory_path = subcategory.xpath(
                    self.config.get(SUBCATEGORY_PATH, "")).extract_first()
            subcategory_path = clean_string(subcategory_path)
            yield {
                'category': response.meta.get("category", ""),
                'subcategory_name': subcategory_name,
                'link': subcategory_path,
                "url_init": sef.url_init
            }
